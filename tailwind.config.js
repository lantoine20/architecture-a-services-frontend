module.exports = {
  content: ["./src/**/*.{html,js}", "./node_modules/flowbite/**/*.js"],
  theme: {
    maxWidth: {
      "1/2": "50%",
    },
    extend: {
      padding: {
        "50px": "50px",
      },
      scale: {
        104: "1.01",
      },
    },
  },
  plugins: [require("flowbite/plugin")],
};
