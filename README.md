![This is an image](/public/screenshot.jpg)

<aside>
📌 Cette page présente le projet informatique à réaliser en Architecture à services
Pour ce projet, j’ai réalisé une todo comprenant plusieurs entités : les personnes ainsi que leurs tâches respectives.
Lien vers le back-end du projet : [https://gitlab.com/lantoine20/architecture-a-services-backend](https://gitlab.com/lantoine20/architecture-a-services-backend)

Accès rapide :

</aside>

---

# 🔧 Installation

> ⚠ Require an installation of hsqldb. Run hsqldb on localhost:8080
> 

```bash
Open a terminal in hsqldb folder and run the following command :
java -cp ./lib/hsqldb.jar org.hsqldb.server.Server --database.0 file:firstBDD --dbname.0 firstBDD
```

### Front-end

> Pensez à lancer le back-end d'abord

```bash
git clone https://gitlab.com/lantoine20/architecture-a-services-frontend.git
cd architecture-a-services-fronted
npm run start
```

---

# 📜 Description

> Pour ce projet, j’ai réalisé une todo comprenant plusieurs entités : les personnes ainsi que leurs tâches respectives.
Une personne est une entité composée d’un prénom.
Une todo est une entité composée d’un nom, d’une description, d’une priorité ainsi que d’une personne.
Il est possible de créer ou supprimer une personne, de lui ajouter/supprimer une ou plusieurs todo(s). Il est également possible d’éditer une todo déjà existante afin de changer son nom, sa description ou sa priorité.
> 

---

# 🗝 Fonctionnement BDD

> **Personne:** 
ID (Integer PRIMARY KEY, identifiant de la personne généré automatiquement), Prénom (Champ de texte, prénom de la personne pour l’identifier dans l’app)
**Todo:**
ID (Integer PRIMARY KEY, identifiant de la todo généré automatiquement), Nom (Champ de texte, nom de la todo pour l’identifier dans l’app), Description (Champ de texte pour détailler la todo), Priorité (String {impératif, modéré, peu urgent} afin de déterminer les priorités), Personne_ID (identifiant de la personne à qui la todo est associée)
**Relation**: 
Une todo est associée à une personne, chaque personne possède une liste de todos.
> 
