import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faUser } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

//Navbar du site
class Navbar extends Component {
  render() {
    return (
      <nav className="flex items-center justify-between flex-wrap bg-gray-700 p-6 mb-8">
      //Logo du site avec un lien vers la homepage
        <Link to="/">
          <span className="font-semibold text-2xl text-white tracking-tight ">
            <p className="animate-pulse">MyToDo.com</p>
          </span>
        </Link>
        <div className="block lg:hidden">
          <button className="flex items-center px-3 py-2 border rounded text-teal-200 border-teal-400 hover:text-white hover:border-white">
            <svg
              class="fill-current h-3 w-3"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <title>Menu</title>
              <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
            </svg>
          </button>
        </div>
        <div className="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
          <div className="text-sm lg:flex-grow"></div>
          <div>
          //Button avec un lien vers la création de personne
            <Link to={"/welcome"}>
              <div className="flex items-center">
                <button className="inline-block text-xl px-3 py-3 leading-none border rounded-3xl text-white border-indigo-400 bg-indigo-400 hover:border-indigo-800 hover:text-white hover:bg-indigo-800 mt-4 lg:mt-0 hover:animate-bounce">
                  <FontAwesomeIcon icon={faUser} className="" />

                  <FontAwesomeIcon icon={faPlus} className="ml-2" />
                </button>
              </div>
            </Link>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navbar;
