import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import Navbar from "./Navbar";

//Composant permettant de créer ou éditer une todo
//La page prendra en compte s'il s'agit d'une nouvelle todo à créer ou d'une todo déjà existante à éditer
//Contient un formulaire relié au back afin de pouvoir POST et créer une todo ou PUT et éditer une todo.
class TodoEdit extends Component {
  emptyItem = {
    name: "",
    description: "",
    urgent: "urgent",
  };

  constructor(props) {
    super(props);
    this.state = {
      item: this.emptyItem,
      idmyTodo: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    if (this.props.match.params.id !== "new") {
      const todos = await (
        await fetch(`/api/${this.props.match.params.id}`)
      ).json();
      this.setState({ item: todos });
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let item = { ...this.state.item };
    item[name] = value;
    this.setState({ item });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const { item } = this.state;
    await fetch("/personnes/" + this.props.location.search.match(/(\d+)/)[0])
      .then((response) => response.json())
      .then((personne) =>
        this.setState({ item: { ...item, personne: personne } }, () => {
          console.log(this.state.item);
        })
      );
    await fetch("/api/" + (item.id ? item.id : ""), {
      method: item.id ? "PUT" : "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(this.state.item),
    }).then((response) =>
      response.json().then((data) => {
        this.setState({ idmyTodo: data.id });
      })
    );
    this.props.history.push("/");
  }

  render() {
    const { item } = this.state;

    return (
      <div className="iscentered h-screen bg-gray-800">
        <Navbar />
        <div className="max-w-[50%]">
          <form className="rounded px-8 pt-6 pb-8 mb-4">
            <div className="mb-4">
              <label
                className="block text-indigo-400 text-sm font-bold mb-2"
                for="username"
              >
                Name
              </label>
              <input
                className="shadow appearance-none border-2 bg-gray-100 border-gray-400 rounded-xl w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:border-none focus:ring-2 focus:ring-indigo-400 focus:bg-white focus:animate-pulse"
                type="text"
                name="name"
                id="name"
                autoFocus
                placeholder={item.name !== "" ? item.name : "Choisir un nom"}
                value={item.name || ""}
                onChange={this.handleChange}
                autoComplete="name"
              />
            </div>
            <div className="mb-6">
              <label
                class="block text-indigo-400 text-sm font-bold mb-2"
                for="text"
              >
                Description
              </label>
              <input
                className="shadow appearance-none border-2 bg-gray-100 border-gray-400 rounded-xl w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:border-none focus:ring-2 focus:ring-indigo-400 focus:bg-white focus:animate-pulse"
                type="text"
                name="description"
                id="description"
                value={item.description || ""}
                placeholder={
                  item.description !== ""
                    ? item.description
                    : "Ajouter une description"
                }
                onChange={this.handleChange}
                autoComplete="description"
              />
            </div>
            <div className="mb-6">
              <label
                className="block text-indigo-400 text-sm font-bold mb-2"
                for="text"
              >
                Prioritée
              </label>
              <select id="urgent" name="urgent" onChange={this.handleChange}>
                <option value="urgent">Impératif</option>
                <option value="moyurgent">Modéré</option>
                <option value="nourgent">Peu urgent</option>
              </select>
            </div>

            <div className="items-center space-x-8 ">
              <button
                className="font-bold text-lg  bg-indigo-400 rounded text-white py-2 px-2 border border-transparent hover:bg-transparent hover:border-indigo-400 hover:text-indigo-400 focus:outline-none focus:ring-2 focus:ring-indigo-400 hover:scale-125"
                type="submit"
                onClick={this.handleSubmit}
              >
                Valider
              </button>
              <Link to="/">
                <button className="font-bold text-lg  bg-indigo-400 rounded text-white py-2 px-2 border border-transparent hover:bg-transparent hover:border-indigo-400 hover:text-indigo-400 focus:outline-none focus:ring-2 focus:ring-indigo-400 hover:scale-125">
                  Annuler
                </button>
              </Link>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
export default withRouter(TodoEdit);
