import React, { Component } from "react";
import Navbar from "./Navbar";
import TodoList from "./TodoList";

//Home Component affichant une Navbar ainsi que la liste des Personnes/Todos
class Home extends Component {
  render() {
    return (
      <div className="bg-gray-800 h-screen">
        <Navbar />
        <TodoList />
      </div>
    );
  }
}

export default Home;
