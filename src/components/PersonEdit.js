import React, { Component } from "react";
import Navbar from "./Navbar";
import { Link } from "react-router-dom";


//Composant permettant de créer une personne
//Contient un formulaire relié au back afin de pouvoir POST et créer un personne.
class EditList extends Component {
  emptyItem = {
    prenom: "",
    age: 18,
    todos: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      item: this.emptyItem,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let item = { ...this.state.item };
    item[name] = value;
    this.setState({ item });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const { item } = this.state;
    await fetch("/personnes/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(item),
    });

    this.props.history.push("/");
  }

  render() {
    return (
      <div className="iscentered h-screen bg-gray-800">
        <Navbar />
        <div className="max-w-[50%]">
          <form className="rounded px-8 pt-6 pb-8 mb-4">
            <div className="mb-4">
              <label
                className="block text-indigo-400 text-sm font-bold mb-2"
                for="username"
              >
                Prénom
              </label>
              <input
                className="shadow appearance-none border-2 bg-gray-100 border-gray-400 rounded-xl w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:border-none focus:ring-2 focus:ring-indigo-400 focus:bg-white focus:animate-pulse"
                type="text"
                name="prenom"
                id="prenom"
                autoFocus
                onChange={this.handleChange}
                autoComplete="prenom"
              />
            </div>

            <div className="items-center space-x-8 ">
              <button
                className="font-bold text-lg  bg-indigo-400 rounded text-white py-2 px-2 border border-transparent hover:bg-transparent hover:border-indigo-400 hover:text-indigo-400 focus:outline-none focus:ring-2 focus:ring-indigo-400 hover:scale-125"
                type="submit"
                onClick={this.handleSubmit}
              >
                Valider
              </button>
              <Link to="/">
                <button className="font-bold text-lg  bg-indigo-400 rounded text-white py-2 px-2 border border-transparent hover:bg-transparent hover:border-indigo-400 hover:text-indigo-400 focus:outline-none focus:ring-2 focus:ring-indigo-400 hover:scale-125">
                  Annuler
                </button>
              </Link>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
export default EditList;
