import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlus,
  faPenToSquare,
  faTrashCan,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

//Composent permettant de récupérer toutes les personnes/todos de la base de données via des méthodes GET.
//Chaque personne/todo a un bouton de suppression associé, ici aussi relié au back afin de pouvoir DELETE.
//Concernant l'affichage, chaque personne est affichée dans une section différentes et ses todos viennent s'afficher en dessous.

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = { todos: [], personnes: [] };
    this.remove = this.remove.bind(this);
  }

  componentDidMount() {
    fetch("/api/todos")
      .then((response) => response.json())
      .then((data) => this.setState({ todos: data }));
    fetch("/personnes")
      .then((response) => response.json())
      .then((personnes) => this.setState({ personnes: personnes }));
  }

  async remove(id) {
    await fetch(`/api/${id}`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then(() => {
      let updatedTodos = [...this.state.todos].filter((i) => i.id !== id);
      this.setState({ todos: updatedTodos });
    });
  }

  async removePerson(id) {
    await fetch(`/personnes/${id}`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then(() => {
      let updatedPersonnes = [...this.state.personnes].filter(
        (i) => i.id !== id
      );
      this.setState({ personnes: updatedPersonnes });
    });
  }

  render() {
    const { personnes } = this.state;
    return (
      <div className="flex flex-col items-center place-content-center">
        {personnes.map((personne) => (
          <div className="mt-9 w-2/3 flex flex-col bg-indigo-100 p-4 rounded-xl shadow-lg shadow-indigo-500/50 ">
            <div className="flex items-center justify-between">
              <div className="flex items-center space-x-4">
                <div className="text-2xl font-bold">{personne.prenom}</div>
              </div>
              <div className="flex items-center space-x-4 text-2xl text-indigo-700">
                <Link to={"/new" + "?user=" + personne.id}>
                  <button className="">
                    <FontAwesomeIcon
                      icon={faPlus}
                      className="ml-2 hover:animate-bounce"
                    />
                  </button>
                </Link>

                <button
                  type="button"
                  className=""
                  onClick={() => this.removePerson(personne.id)}
                >
                  <FontAwesomeIcon
                    icon={faTrashCan}
                    className="ml-2 hover:animate-bounce"
                  />
                </button>
              </div>
            </div>

            <div className="space-y-4 flex flex-col items-center">
              {personne.todos.map((todo) => (
                <div className="bg-gray-700 text-white w-full max-w-md flex flex-col rounded-xl shadow-md shadow-indigo-500/50 p-4 w-3/4 hover:scale-104">
                  <div className="flex items-center justify-between">
                    <div className="flex items-center space-x-4">
                      <div className="text-2xl font-bold ">{todo.name}</div>
                      {todo.urgent == "urgent" ? (
                        <div className="border border-red-300 p-2 text-red-400 bg-red-100 rounded-3xl text-sm">
                          impératif
                        </div>
                      ) : todo.urgent == "nourgent" ? (
                        <div className="border border-green-300 p-2 text-green-400 bg-green-100 rounded-3xl text-sm">
                          peu urgent
                        </div>
                      ) : (
                        <div className="border border-yellow-300 p-2 text-yellow-400 bg-yellow-100 rounded-3xl text-sm">
                          modéré
                        </div>
                      )}
                    </div>
                    <div className="flex items-center space-x-4">
                      <div className="cursor-pointer">
                        <Link to={"/" + todo.id + "?user=" + personne.id}>
                          <button type="button" tag={Link}>
                            <FontAwesomeIcon
                              icon={faPenToSquare}
                              className="ml-2 text-indigo-400 hover:animate-bounce"
                            />
                          </button>
                        </Link>
                      </div>
                      <div className="cursor-pointer">
                        <Link to="/">
                          <button
                            type="button"
                            onClick={() => {
                              this.remove(todo.id);
                              window.location.reload();
                            }}
                          >
                            <FontAwesomeIcon
                              icon={faXmark}
                              className="ml-2 text-indigo-400 hover:animate-bounce"
                            />
                          </button>
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="mt-4 text-gray-400 font-bold text-md">
                    {todo.description}
                  </div>
                </div>
              ))}
              <div className="w-32 border-t border-gray-300"></div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
export default TodoList;
