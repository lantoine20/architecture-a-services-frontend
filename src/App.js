import React, { Component } from "react";
import "./App.css";
import Home from "./components/Home";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import TodoEdit from "./components/TodoEdit";
import PersonEdit from "./components/PersonEdit";

class App extends Component {
  render() {
    return (
      //Router avec Switches permettant de naviguer sur le site vers les différentes pages
      <Router>
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/welcome" component={PersonEdit}></Route>
          <Route path="/:id" component={TodoEdit} />
        </Switch>
      </Router>
    );
  }
}

export default App;
